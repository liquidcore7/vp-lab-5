﻿using System.Xml;

namespace Lab5
{
    public interface XmlPersistable
    {
        string DumpFileName();

        XmlDocument ToDocument();
        void FromDocument(XmlDocument document);
    }
}
