﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace Lab5
{
    public partial class MainWindow : Form, XmlPersistable
    {

        private RadioButton[] radioButtons;
        private bool saveAtShutdown = true;

        public MainWindow()
        {
            InitializeComponent();
            radioButtons = new RadioButton[]
            {
                radioButton1,
                radioButton2,
                radioButton3,
                radioButton4
            };

            Load += (object sender, EventArgs args) =>
            {
                XmlPersistanceService.Load(this);
            };

            clearBtn.Click += (object sender, EventArgs args) =>
            {
                XmlPersistanceService.ClearState(this);
                saveAtShutdown = false;
            };

            FormClosed += (object sender, FormClosedEventArgs args) =>
            {
                if (saveAtShutdown)
                {
                    XmlPersistanceService.Save(this);
                }
            };
        }

        public string DumpFileName()
        {
            return "lab5_mainwindow";
        }

        public void FromDocument(XmlDocument document)
        {
            XmlNode checkedItems = document.GetElementsByTagName("CheckedItems")[0];

            foreach (XmlElement child in checkedItems.ChildNodes)
            {
                int checkedIdx = Convert.ToInt32(child.GetAttribute("index"));
                checkedListBox.SetItemChecked(checkedIdx, true);
            }

            var checkedRadioButtons = document.GetElementsByTagName("CheckedRadioButton");
            if (checkedRadioButtons.Count == 1)
            {
                int idx = Convert.ToInt32(checkedRadioButtons[0].Attributes["index"].Value);
                radioButtons[idx].Checked = true;
            }

            var textBoxes = document.GetElementsByTagName("RichText")[0];
            richTextBox.Rtf = textBoxes.Attributes["value"].Value;

            var checkedGooseParts = document.GetElementsByTagName("CheckedGoosePart");
            if (checkedGooseParts.Count == 1)
            {
                int idx = Convert.ToInt32(checkedGooseParts[0].Attributes["index"].Value);
                listBox.SelectedIndex = idx;
            }

        }

        public XmlDocument ToDocument()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("root");
            root.SetAttribute("id", "root");
            doc.AppendChild(root);

            XmlElement checkedListItems = doc.CreateElement("CheckedItems");
            for (int i = 0; i < checkedListBox.Items.Count; ++i)
            {
                if (checkedListBox.GetItemChecked(i))
                {
                    XmlElement itemNode = doc.CreateElement("item");
                    itemNode.SetAttribute("index", i.ToString());
                    itemNode.SetAttribute("value", checkedListBox.Items[i].ToString());
                    checkedListItems.AppendChild(itemNode);
                }
            }
            root.AppendChild(checkedListItems);

            int checkedRadioIdx = Array.FindIndex(radioButtons, btn => btn.Checked);
            if (checkedRadioIdx >= 0)
            {
                XmlElement checkedRadioButton = doc.CreateElement("CheckedRadioButton");
                checkedRadioButton.SetAttribute("index", checkedRadioIdx.ToString());
                checkedRadioButton.SetAttribute("value", radioButtons[checkedRadioIdx].Text);
                root.AppendChild(checkedRadioButton);
            }

            XmlElement writtenText = doc.CreateElement("RichText");
            writtenText.SetAttribute("value", richTextBox.Rtf);
            root.AppendChild(writtenText);


            int checkedGoosePartIdx = listBox.SelectedIndex;
            if (checkedGoosePartIdx >= 0)
            {
                XmlElement checkedGoosePart = doc.CreateElement("CheckedGoosePart");
                checkedGoosePart.SetAttribute("index", checkedGoosePartIdx.ToString());
                root.AppendChild(checkedGoosePart);
            }

            return doc;
        }

    }
}
