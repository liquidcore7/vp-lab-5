﻿using System;
using System.IO;
using System.Xml;

namespace Lab5
{
    public static class XmlPersistanceService
    {
        private static string directory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\lab5_saves\\";
        private static string DumpFileName(XmlPersistable persistable)
        {
            return directory + persistable.DumpFileName() + ".xml";
        }

        public static void Load(XmlPersistable obj)
        {
            try
            {
                string filename = DumpFileName(obj);
                XmlDocument doc = new XmlDocument();

                doc.Load(filename);
                obj.FromDocument(doc);
            } catch (Exception e)
            {

            }
        }

        public static void ClearState(XmlPersistable obj)
        {
            try
            {
                File.Delete(DumpFileName(obj));
            } catch (Exception e)
            {
                // ignore
            }
        }

        public static void Save(XmlPersistable obj)
        {
            string filename = DumpFileName(obj);
            XmlDocument doc = obj.ToDocument();

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (Stream file = File.Create(filename))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(file))
                {
                    doc.WriteTo(xmlWriter);
                }
            }

        }
    }
}
